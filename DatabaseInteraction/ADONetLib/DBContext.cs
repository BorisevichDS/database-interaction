﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace DBAccess
{
    /// <summary>
    /// Класс описывает контекст данных.
    /// </summary>
    class DBContext
    {
        public SqlConnection Connection { get; }

        public DBContext()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }
    }
}
