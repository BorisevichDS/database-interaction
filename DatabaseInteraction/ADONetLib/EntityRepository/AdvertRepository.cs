﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertRepository : IAdvertRepository
    {
        private DBContext _dbContext;

        public AdvertRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
        }

        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(Advert item)
        {
            string query = "dbo.sp_add_advert";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@seller_id", item.SellerId);
            command.Parameters.AddWithValue("@description", item.Description);
            command.Parameters.AddWithValue("@city_id", item.CityId);
            command.Parameters.AddWithValue("@advert_type_id", item.AdvertTypeId);

            SqlParameter idParameter = new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParameter);

            command.ExecuteNonQuery();

            int.TryParse(idParameter.Value.ToString(), out int itemId);

            return itemId;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(Advert item)
        {
            string query = "dbo.sp_update_advert";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@seller_id", item.SellerId);
            command.Parameters.AddWithValue("@description", item.Description);
            command.Parameters.AddWithValue("@city_id", item.CityId);
            command.Parameters.AddWithValue("@advert_type_id", item.AdvertTypeId);
            command.Parameters.AddWithValue("@id", item.Id);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(Advert item)
        {
            string query = "dbo.sp_delete_advert";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@id", item.Id);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<Advert> GetAll()
        {
            List<Advert> items = new List<Advert>();

            string query = "select id, sellerid, [description], cityid, adverttypeid from dbo.advert;";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        int sellerId = reader.GetInt32(1);
                        string description = reader.GetString(2);
                        int cityId = reader.GetInt32(3);
                        int advertTypeId = reader.GetInt32(4);

                        items.Add(new Advert(id, sellerId, cityId, advertTypeId, description));
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Advert GetById(int id)
        {
            Advert item = null;

            string query = "select top(1) sellerid, [description], cityid, adverttypeid from dbo.advert where id = @id;";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", id);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int sellerId = reader.GetInt32(0);
                        string description = reader.GetString(1);
                        int cityId = reader.GetInt32(2);
                        int advertTypeId = reader.GetInt32(3);

                        item = new Advert(id, sellerId, cityId, advertTypeId, description);
                    }
                }
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает все объявления в указанном городу.
        /// </summary>
        /// <param name="cityName">Название города, в котором необходимо найти объявления.</param>
        /// <returns></returns>
        public List<Advert> GetAdvertsInCity(string cityName)
        {
            List<Advert> items = new List<Advert>();

            string query = "select" + Environment.NewLine +
                           "a.id, a.SellerId, a.[Description], a.CityId, a.AdvertTypeId " + Environment.NewLine +
                           "from dbo.Advert as a " + Environment.NewLine +
                           "join dictionary.City as c on a.CityId = c.Id" + Environment.NewLine +
                           "where c.Name = @cityName;";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@cityName", cityName);

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    int sellerId = reader.GetInt32(1);
                    string description = reader.GetString(2);
                    int cityId = reader.GetInt32(3);
                    int advertTypeId = reader.GetInt32(4);

                    items.Add(new Advert(id, sellerId, cityId, advertTypeId, description));
                }
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает все объявления, созданные конкретным пользователем.
        /// </summary>
        /// <param name="user">Владелец объявлений.</param>
        /// <returns></returns>
        public List<Advert> GetAdvertsFromUser(User user)
        {
            List<Advert> items = new List<Advert>();

            string query = "select" + Environment.NewLine +
                           "a.id, a.SellerId, a.[Description], a.CityId, a.AdvertTypeId " + Environment.NewLine +
                           "from dbo.Advert as a" + Environment.NewLine +
                           "join dbo.[User] as u on a.SellerId = u.Id" + Environment.NewLine +
                           "where u.FirstName = @firstName" + Environment.NewLine +
                           "and u.LastName = @lastName";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@firstName", user.FirstName);
            command.Parameters.AddWithValue("@lastName", user.LastName);

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    int sellerId = reader.GetInt32(1);
                    string description = reader.GetString(2);
                    int cityId = reader.GetInt32(3);
                    int advertTypeId = reader.GetInt32(4);

                    items.Add(new Advert(id, sellerId, cityId, advertTypeId, description));
                }
            }
            return items;
        }
    }
}
