﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertTypeRepository : IAdvertTypeRepository
    {
        private DBContext _dbContext;

        public AdvertTypeRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
        }

        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(AdvertType item)
        {
            string query = "dictionary.sp_add_advert_type";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@name", item.Name);
            command.Parameters.AddWithValue("@code", item.UniqueCode);

            SqlParameter idParameter = new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParameter);

            command.ExecuteNonQuery();

            int.TryParse(idParameter.Value.ToString(), out int itemId);

            return itemId;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(AdvertType item)
        {
            string query = "dictionary.sp_update_advert_type";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@code", item.UniqueCode);
            command.Parameters.AddWithValue("@name", item.Name);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(AdvertType item)
        {
            string query = "dictionary.sp_delete_advert_type";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@code", item.UniqueCode);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<AdvertType> GetAll()
        {
            List<AdvertType> items = new List<AdvertType>();

            string query = "select id, [name], uniquecode from dictionary.AdvertType;";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        string uniqueCode = reader.GetString(2);

                        items.Add(new AdvertType(id, uniqueCode, name));
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AdvertType GetById(int id)
        {
            AdvertType item = null;

            string query = "select [name], uniquecode from dictionary.AdvertType where id = @id";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", (byte)id);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string name = reader.GetString(0);
                        string uniqueCode = reader.GetString(1);

                        item = new AdvertType(id, uniqueCode, name);
                    }
                }
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        public int GetId(AdvertType item)
        {
            int id = default;
            string query = "select top(1) id from dictionary.AdvertType where uniquecode = @code;";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@code", item.UniqueCode);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            return id;
        }
    }
}
