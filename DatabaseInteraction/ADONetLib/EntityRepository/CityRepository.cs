﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности город.
    /// </summary>
    public class CityRepository : ICityRepository
    {
        private DBContext _dbContext;

        public CityRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
        }

        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(City item)
        {
            string query = "insert into dictionary.city([name]) values (@name); set @id = scope_identity()";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.Add("@name", SqlDbType.VarChar, 500);
            command.Parameters["@name"].Value = item.Name;

            SqlParameter idParameter = new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParameter);

            command.ExecuteNonQuery();

            int.TryParse(idParameter.Value.ToString(), out int itemId);
            return itemId;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(City item)
        {
            string query = "update c" +
                           "set [name] = @name" +
                           "from dictionary.city as c" +
                           "where c.id = @id";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", item.Id);
            command.Parameters.AddWithValue("@name", item.Name);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(City item)
        {
            string query = "delete c " + 
                            "from dictionary.city as c " +
                            "where c.[name] = @name";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@name", item.Name);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<City> GetAll()
        {
            List<City> items = new List<City>();

            string query = "select id, [name] from dictionary.city";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        items.Add(new City(reader.GetInt32(0), reader.GetString(1)));
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public City GetById(int id)
        {
            City item = null;

            string query = "select id, [name] from dictionary.city where id = @id";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", id);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        item = new City(reader.GetInt32(0), reader.GetString(1));
                    }
                }
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(City item)
        {
            int id = default;
            string query = "select top(1) id from dictionary.city where [name] = @name;";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@name", item.Name);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            return id;
        }
    }
}
