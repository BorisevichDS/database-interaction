﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class RatingRepository : IRatingRepository
    {
        private DBContext _dbContext;

        public RatingRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
        }

        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(Rating item)
        {
            string query = "dbo.sp_add_rating";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@from_user_id", item.UserFromId);
            command.Parameters.AddWithValue("@to_user_id", item.UserToId);
            command.Parameters.AddWithValue("@rating_type_id", item.RatingTypeId);
            command.Parameters.AddWithValue("@comment", item.Comment);

            SqlParameter idParameter = new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParameter);

            command.ExecuteNonQuery();

            int.TryParse(idParameter.Value.ToString(), out int itemId);

            return itemId;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(Rating item)
        {
            string query = "dbo.sp_update_rating";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@id", item.Id);
            command.Parameters.AddWithValue("@from_user_id", item.UserFromId);
            command.Parameters.AddWithValue("@to_user_id", item.UserToId);
            command.Parameters.AddWithValue("@rating_type_id", item.RatingTypeId);
            command.Parameters.AddWithValue("@comment", item.Comment);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(Rating item)
        {
            string query = "dbo.sp_delete_rating";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@id", item.Id);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<Rating> GetAll()
        {
            List<Rating> items = new List<Rating>();

            string query = "select id, fromuserid, touserid, ratingtypeid, comment from dbo.rating";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        int userFromId = reader.GetInt32(1);
                        int userToId = reader.GetInt32(2);
                        byte ratingTypeId = reader.GetByte(3);
                        string comment = reader.GetString(4);

                        items.Add(new Rating(id, userFromId, userToId, ratingTypeId, comment));
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Rating GetById(int id)
        {
            Rating item = null;

            string query = "select fromuserid, touserid, ratingtypeid, comment from dbo.rating where id = @id;";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", id);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int userFromId = reader.GetInt32(0);
                        int userToId = reader.GetInt32(1);
                        byte ratingTypeId = reader.GetByte(2);
                        string comment = reader.GetString(3);

                        item = new Rating(id, userFromId, userToId, ratingTypeId, comment);
                    }
                }
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает среднюю оценку для пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns></returns>
        public double GetAverageRatingForUser(User user)
        {
            double avgRating = default;

            string query = "select" + Environment.NewLine +
                            "avg(rt.Mark)" + Environment.NewLine +
                            "from dbo.Rating as r" + Environment.NewLine +
                            "join dbo.[User] as u on r.ToUserId = u.Id" + Environment.NewLine +
                            "join dictionary.RatingType as rt on r.RatingTypeId = rt.Id" + Environment.NewLine +
                            "where u.FirstName = @firstName" + Environment.NewLine +
                            "and u.LastName = @lastName";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@firstName", user.FirstName);
            command.Parameters.AddWithValue("@lastName", user.LastName);

            var ret = command.ExecuteScalar().ToString();

            double.TryParse(ret, out avgRating);

            return avgRating;
        }
    }
}
