﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип рейтинга".
    /// </summary>
    public class RatingTypeRepository : IRatingTypeRepository
    {
        private DBContext _dbContext;

        public RatingTypeRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
        }

        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(RatingType item)
        {
            string query = "dictionary.sp_add_rating_type";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@mark", item.Mark);
            command.Parameters.AddWithValue("@description", item.Description);

            SqlParameter idParameter = new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.TinyInt,
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(idParameter);

            command.ExecuteNonQuery();

            int.TryParse(idParameter.Value.ToString(), out int itemId);
            return itemId;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(RatingType item)
        {
            string query = "dictionary.sp_update_rating_type";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@mark", item.Mark);
            command.Parameters.AddWithValue("@description", item.Description);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(RatingType item)
        {
            string query = "dictionary.sp_delete_rating_type";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@mark", item.Mark);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<RatingType> GetAll()
        {
            List<RatingType> items = new List<RatingType>();

            string query = "select id, mark, [description] from dictionary.RatingType";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        byte id = reader.GetByte(0);
                        byte mark = reader.GetByte(1);
                        string description = reader.GetString(2);

                        items.Add(new RatingType(id, mark, description));
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RatingType GetById(int id)
        {
            RatingType item = null;

            string query = "select mark, [description] from dictionary.RatingType where id = @id";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@id", (byte)id);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        byte mark = reader.GetByte(0);
                        string description = reader.GetString(1);

                        item = new RatingType((byte)id, mark, description);
                    }
                }
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(RatingType item)
        {
            int id = default;
            string query = "select top(1) id from dictionary.RatingType where mark = @mark;";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@mark", item.Mark);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = (int)reader.GetByte(0);
                    }
                }
            }
            return id;
        }
    }
}
