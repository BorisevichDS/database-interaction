﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Пользователь".
    /// </summary>
    public class UserRepository : IUserRepository
    {
        private DBContext _dbContext;
        private SqlDataAdapter _sqlDataAdapter;
        private DataTable _dataTable = new DataTable();

        public UserRepository()
        {
            _dbContext = new DBContext();
            _dbContext.Connection.Open();
            Initialize();
        }

        /// <summary>
        /// Метод инициализирует параметры для вставки, обновление и удаления данных.
        /// </summary>
        private void Initialize()
        {
            _sqlDataAdapter = new SqlDataAdapter();
            SqlCommandBuilder builder = new SqlCommandBuilder(_sqlDataAdapter);

            #region Команда для получения строк таблицы
            string selectQuery = "select id, firstname, lastname, createdon from dbo.[user];";
            SqlCommand selectCommand = new SqlCommand(selectQuery, _dbContext.Connection);
            _sqlDataAdapter.SelectCommand = selectCommand;
            #endregion

            #region Команда для вставки строк
            // Определяем команду для вставки строк
            SqlCommand insertCommand = new SqlCommand("dbo.sp_add_user");
            insertCommand.CommandType = CommandType.StoredProcedure;

            // Входные параметры для команды вставки строк
            SqlParameter firstNameParameter = new SqlParameter("@first_name", SqlDbType.NVarChar, 100, "firstname");
            SqlParameter lastNameParameter = new SqlParameter("@last_name", SqlDbType.NVarChar, 100, "lastname");

            // Выходные параметры для команды вставки строк
            SqlParameter idParameter = new SqlParameter("@id", SqlDbType.Int, 8, "id") { Direction = ParameterDirection.Output };

            // Связываем команду для вставки строк и параметры
            insertCommand.Parameters.AddRange(new SqlParameter[] { firstNameParameter, lastNameParameter, idParameter });

            // Привязываем команду для вставки строк к адаптеру
            _sqlDataAdapter.InsertCommand = insertCommand;
            #endregion

            #region Команда для обновления строк
            string updateQuery = "dbo.sp_update_user";
            SqlCommand updateCommand = new SqlCommand(updateQuery, _dbContext.Connection);
            updateCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter idParameterUpd = new SqlParameter("@id", SqlDbType.Int, 4, "id");
            SqlParameter firstNameParameterUpd = new SqlParameter("@first_name", SqlDbType.NVarChar, 100, "firstname");
            SqlParameter lastNameParameterUpd = new SqlParameter("@last_name", SqlDbType.NVarChar, 100, "lastname");

            updateCommand.Parameters.AddRange(new SqlParameter[] { idParameterUpd, firstNameParameterUpd, lastNameParameterUpd });

            _sqlDataAdapter.UpdateCommand = updateCommand;
            #endregion

            #region Команда для удаления строк
            string deleteQuery = "dbo.sp_delete_user";
            SqlCommand deleteCommand = new SqlCommand(deleteQuery, _dbContext.Connection);

            SqlParameter idParameterDel = new SqlParameter("@id", SqlDbType.Int);
            deleteCommand.Parameters.Add(idParameterDel);

            _sqlDataAdapter.DeleteCommand = deleteCommand;
            #endregion
        }

        /// <summary>
        /// Метод актуализирует слепок данных.
        /// </summary>
        private void RefreshDataTable()
        {
            if (_dataTable.Rows.Count > 0)
            {
                _dataTable.Clear();
            }

            _sqlDataAdapter?.Fill(_dataTable);
        }

        /// <summary>
        /// Метод создает запись о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные о котором необходимо сохранить.</param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(User item)
        {
            int userId = default;

            RefreshDataTable();

            if (_sqlDataAdapter != null)
            {
                // Добавляем новую строку
                DataRow row = _dataTable.NewRow();
                row["firstname"] = item.FirstName;
                row["lastname"] = item.LastName;
                _dataTable.Rows.Add(row);

                // Отправляем изменения в БД
                _sqlDataAdapter.Update(_dataTable);

                // Сохраняем изменения
                _dataTable.AcceptChanges();

                var lastRow = _dataTable.AsEnumerable().Select(rw => new { value = rw["id"].ToString() }).OrderByDescending(r => r.value).FirstOrDefault();
                int.TryParse(lastRow.value, out userId);
            }
            return userId;
        }

        /// <summary>
        /// Метод обновляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо обновить.</param>
        public void Update(User item)
        {
            RefreshDataTable();

            if (_sqlDataAdapter != null)
            {
                DataRow row = _dataTable.AsEnumerable().Where(r => r.Field<int>("id") == item.Id).First<DataRow>();
                row["firstname"] = item.FirstName;
                row["lastname"] = item.LastName;

                // Отправлем изменения в БД
                _sqlDataAdapter.Update(_dataTable);

                // Сохраняем изменения
                _dataTable.AcceptChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо удалить.</param>
        public void Delete(User item)
        {
            if (_sqlDataAdapter != null)
            {
                DataRow row = _dataTable.AsEnumerable().Where(r => r.Field<int>("id") == item.Id).First<DataRow>();
                _dataTable.Rows.Remove(row);

                // Отправлем изменения в БД
                _sqlDataAdapter.Update(_dataTable);

                // Сохраняем изменения
                _dataTable.AcceptChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех пользователей.
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            List<User> items = new List<User>();

            string query = "select id, firstname, lastname from dbo.[user];";
            SqlCommand command = new SqlCommand(query, _dbContext.Connection);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int.TryParse(row["id"].ToString(), out int id);
                string firstName = row["firstname"].ToString();
                string lastName = row["lastname"].ToString();

                items.Add(new User(id, firstName, lastName));
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает пользователя по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        /// <returns></returns>
        public User GetById(int id)
        {
            User item = null;

            RefreshDataTable();

            DataRow row = _dataTable.AsEnumerable().Where(r => r.Field<int>("id") == id).First<DataRow>();

            if (row != null)
            {
                string firstName = row.Field<string>("firstname");
                string lastName = row.Field<string>("lastname");
                item = new User(id, firstName, lastName);
            }
            return item;
        }

        public void Dispose()
        {
            _dbContext.Connection?.Close();
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Пользователь, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(User item)
        {
            int id = default;
            string query = "select top(1) id from dbo.[user] where firstname = @first_name and lastname = @last_name;";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@first_name", item.FirstName);
            command.Parameters.AddWithValue("@last_name", item.LastName);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            return id;
        }

        /// <summary>
        /// Метод ищет всех пользователей с заданным именем.
        /// </summary>
        /// <param name="searchedName">Имя пользователя.</param>
        /// <returns>Список пользователей с соответствующим именем.</returns>
        public List<User> FindUsersByName(string searchedName)
        {
            List<User> items = new List<User>();

            string query =  "select " + Environment.NewLine +
                            "u.Id, u.FirstName, u.LastName" + Environment.NewLine +
                            "from dbo.[user] as u" + Environment.NewLine +
                            "where u.FirstName = @firstName";

            SqlCommand command = new SqlCommand(query, _dbContext.Connection);
            command.Parameters.AddWithValue("@firstName", searchedName);

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    string firstName = reader.GetString(1);
                    string lastName = reader.GetString(2);

                    items.Add(new User(id, firstName, lastName));
                }
            }
            return items;
        }
    }
}
