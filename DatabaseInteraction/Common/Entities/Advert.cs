﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Common
{
    /// <summary>
    /// Класс описывает сущность "Объявление".
    /// </summary>
    [Table("dbo.Advert")]
    public class Advert
    {
        /// <summary>
        /// Идентификатор записи.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор продавца (владелец объявления).
        /// </summary>
        [Required]
        public int SellerId { get; set; }

        /// <summary>
        /// Идентификатор города, в котором размещено объявление.
        /// </summary>
        [Required]
        public int CityId { get; set; }

        /// <summary>
        /// Идентификатор типа объявления.
        /// </summary>
        [Required]
        public int AdvertTypeId { get; set; }

        /// <summary>
        /// Описание объявления.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(4000)]
        public string Description { get; set; }

        public virtual AdvertType AdvertType { get; set; }

        public virtual City City { get; set; }

        public virtual User Seller { get; set; }

        public Advert()
        {

        }
        public Advert(int id, int sellerId, int cityId, int advertTypeId, string description)
        {
            Id = id;
            SellerId = sellerId;
            CityId = cityId;
            AdvertTypeId = advertTypeId;
            Description = description;
        }
        public Advert(int sellerId, int cityId, int advertTypeId, string description)
        {
            SellerId = sellerId;
            CityId = cityId;
            AdvertTypeId = advertTypeId;
            Description = description;
        }

        public override string ToString()
        {
            return $"id = {Id}, description = {Description}";
        }
    }
}
