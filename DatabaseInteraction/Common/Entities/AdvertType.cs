﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    /// <summary>
    /// Класс описывает сущность "Тип объявления".
    /// </summary>
    [Table("dictionary.AdvertType")]
    public class AdvertType
    {
        /// <summary>
        /// Идентификатор типа объявления.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Имя типа объявления.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Уникальный код типа.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string UniqueCode { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }

        public AdvertType()
        {
            Adverts = new HashSet<Advert>();
        }
        public AdvertType(int id, string uniqueCode, string name)
        {
            Id = id;
            UniqueCode = uniqueCode;
            Name = name;
            Adverts = new HashSet<Advert>();
        }
        public AdvertType(string uniqueCode, string name)
        {
            UniqueCode = uniqueCode;
            Name = name;
            Adverts = new HashSet<Advert>();
        }
        public AdvertType(string uniqueCode)
        {
            UniqueCode = uniqueCode;
            Adverts = new HashSet<Advert>();
        }

        public override string ToString()
        {
            return $"Id = {Id}, code = {UniqueCode}, name = {Name}";
        }
    }
}
