﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// Класс описывает сущность город.
    /// </summary>
    [Table("dictionary.city")]
    public class City
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        [Required]
        [Column(TypeName = "varchar")]
        [StringLength(500)]
        public string Name { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }

        public City()
        {
            Adverts = new HashSet<Advert>();
        }

        public City(string name)
        {
            Name = name;
            Adverts = new HashSet<Advert>();
        }
        public City(int id, string name)
        {
            Id = id;
            Name = name;
            Adverts = new HashSet<Advert>();
        }

        public override string ToString()
        {
            return $"Id = {Id}, name = {Name}";
        }
    }
}
