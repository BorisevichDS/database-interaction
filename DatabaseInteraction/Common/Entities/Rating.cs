﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    /// <summary>
    /// Класс описывает сущность "Рейтинг пользователя"
    /// </summary>
    [Table("dbo.Rating")]
    public class Rating
    {
        /// <summary>
        /// Идентификатор записи.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя, поставившего оценку.
        /// </summary>
        [Required]
        [Column("FromUserId")]
        public int UserFromId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, которому поставили оценку.
        /// </summary>
        [Required]
        [Column("ToUserId")]
        public int UserToId { get; set; }

        /// <summary>
        /// Идентификатор типа оценки
        /// </summary>
        [Required]
        public byte RatingTypeId { get; set; }

        /// <summary>
        /// Комментарий к оценке.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(4000)]
        public string Comment { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        public virtual RatingType RatingType { get; set; }

        public virtual User UserFrom { get; set; }

        public virtual User UserTo { get; set; }

        public Rating()
        {

        }
        public Rating(int id, int userFromId, int userToId, byte ratingTypeId, string comment)
        {
            Id = id;
            UserFromId = userFromId;
            UserToId = userToId;
            RatingTypeId = ratingTypeId;
            Comment = comment;
        }
        public Rating(int userFromId, int userToId, byte ratingTypeId, string comment)
        {
            UserFromId = userFromId;
            UserToId = userToId;
            RatingTypeId = ratingTypeId;
            Comment = comment;
        }

        public override string ToString()
        {
            return $"id = {Id}, UserFrom id = {UserFromId}, UserTo id = {UserToId}, " +
                   $"RatigType id = {RatingTypeId}, Comment = {Comment}";
        }
    }
}
