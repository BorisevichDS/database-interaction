﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    /// <summary>
    /// Класс описывает сущность "Тип рейтинга".
    /// </summary>
    [Table("dictionary.RatingType")]
    public class RatingType
    {
        /// <summary>
        /// Идентификатор типа.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        /// <summary>
        /// Числовое представление оценки.
        /// </summary>
        [Required]
        public byte Mark { get; set; }

        /// <summary>
        /// Описание оценки.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(100)]
        public string Description { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public RatingType()
        {
            Ratings = new HashSet<Rating>();
        }
        public RatingType(byte id, byte mark, string description)
        {
            Id = id;
            Mark = mark;
            Description = description;
            Ratings = new HashSet<Rating>();
        }
        public RatingType(byte mark, string description)
        {
            Mark = mark;
            Description = description;
            Ratings = new HashSet<Rating>();
        }
        public RatingType(byte mark)
        {
            Mark = mark;
            Ratings = new HashSet<Rating>();
        }

        public override string ToString()
        {
            return $"Id = {Id}, Mark = {Mark}, Description = {Description}";
        }
    }
}
