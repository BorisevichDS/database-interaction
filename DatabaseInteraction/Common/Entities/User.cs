﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    /// <summary>
    /// Класс описывает сущность пользователь.
    /// </summary>
    [Table("dbo.User")]
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(100)]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя.
        /// </summary>
        [Required]
        [Column(TypeName = "nvarchar")]
        [StringLength(100)]
        public string LastName { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public virtual ICollection<Rating> Ratings1 { get; set; }

        public User()
        {
            Adverts = new HashSet<Advert>();
            Ratings = new HashSet<Rating>();
            Ratings1 = new HashSet<Rating>();
        }
        public User(string firstname, string lastname)
        {
            FirstName = firstname;
            LastName = lastname;
            Adverts = new HashSet<Advert>();
            Ratings = new HashSet<Rating>();
            Ratings1 = new HashSet<Rating>();
        }
        public User(int id, string firstname, string lastname)
        {
            Id = id;
            FirstName = firstname;
            LastName = lastname;
            Adverts = new HashSet<Advert>();
            Ratings = new HashSet<Rating>();
            Ratings1 = new HashSet<Rating>();
        }

        public override string ToString()
        {
            return $"Id = {Id}, FirstName = {FirstName}, LastName = {LastName}";
        }
    }
}
