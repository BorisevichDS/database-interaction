﻿using System;
using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Тип объявления".
    /// </summary>
    public interface IAdvertRepository : IRepository<Advert>, IDisposable
    {
        /// <summary>
        /// Метод возвращает все объявления в указанном городе.
        /// </summary>
        /// <param name="cityName">Название города, в котором необходимо найти объявления.</param>
        /// <returns></returns>
        List<Advert> GetAdvertsInCity(string cityName);

        /// <summary>
        /// Метод возвращает все объявления, созданные конкретным пользователем.
        /// </summary>
        /// <param name="user">Владелец объявлений.</param>
        /// <returns></returns>
        List<Advert> GetAdvertsFromUser(User user);

    }

}
