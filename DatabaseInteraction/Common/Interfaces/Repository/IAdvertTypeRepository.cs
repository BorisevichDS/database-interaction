﻿using System;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Тип объявления".
    /// </summary>
    public interface IAdvertTypeRepository : IRepository<AdvertType>, IDisposable, IDictionary<AdvertType>
    {


    }
}
