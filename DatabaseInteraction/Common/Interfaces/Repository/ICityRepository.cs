﻿using System;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Город".
    /// </summary>
    public interface ICityRepository : IRepository<City>, IDisposable, IDictionary<City>
    {

    }
}
