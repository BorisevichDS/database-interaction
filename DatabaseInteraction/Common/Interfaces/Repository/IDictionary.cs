﻿using System;

namespace Common
{
    /// <summary>
    /// Интерфейс описывает ряд методов, необходимых для работы со справочниками в базе данных.
    /// </summary>
    public interface IDictionary<T> where T: class
    {
        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        int GetId(T item);
    }
}
