﻿using System;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Тип объявления".
    /// </summary>
    public interface IRatingRepository : IRepository<Rating>, IDisposable
    {
        /// <summary>
        /// Метод возвращает среднюю оценку для пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns></returns>
        double GetAverageRatingForUser(User user);

    }
}
