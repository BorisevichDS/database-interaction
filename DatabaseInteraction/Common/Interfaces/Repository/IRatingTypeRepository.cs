﻿using System;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Тип рейтинга".
    /// </summary>
    public interface IRatingTypeRepository : IRepository<RatingType>, IDisposable, IDictionary<RatingType>
    {

    }
}
