﻿using System.Collections.Generic;

namespace Common
{
    public interface IRepository<T> where T: class
    {
        /// <summary>
        /// Метод создает запись о сущности в базе данных.
        /// </summary>
        /// <param name="item">Сущность, данные которой необходимо сохранить в базе данных.</param>
        /// <returns></returns>
        int Create(T item);

        /// <summary>
        /// Метод обновляет информацию о сущности в базе данных.
        /// </summary>
        /// <param name="item">Сущность, данные которой необходимо обновить в базе данных.</param>
        void Update(T item);

        /// <summary>
        /// Метод удаляет информацию о сущности из базы данных.
        /// </summary>
        /// <param name="item">Сущность, данные о которой необходимо удалить из базы данных.</param>
        void Delete(T item);

        /// <summary>
        /// Метод возвращает список всех сущностей заданного типа.
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// Метод ищет данные сущности по её идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор записи в базе данных.</param>
        /// <returns></returns>
        T GetById(int id);


    }
}
