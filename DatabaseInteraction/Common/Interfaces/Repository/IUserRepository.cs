﻿using System;
using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// Интерфейс репозитория для сущности "Пользователь".
    /// </summary>
    public interface IUserRepository : IRepository<User>, IDisposable, IDictionary<User>
    {
        /// <summary>
        /// Метод ищет всех пользователей с заданным именем.
        /// </summary>
        /// <param name="searchedName">Имя пользователя.</param>
        /// <returns>Список пользователей с соответствующим именем.</returns>
        List<User> FindUsersByName(string searchedName);
    }
}
