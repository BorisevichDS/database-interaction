﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBAccess;
using Common;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var quitAnswer = "N";
                // Спрашиваем, что хочет сделать пользователь
                AskFirstQuestion();

                do
                {
                    Console.Write("Введите выбранные вариант: ");
                    string inputString = Console.ReadLine().Trim();
                    if (int.TryParse(inputString, out int variant))
                    {
                        switch (variant)
                        {
                            case 1:
                                AddCity();
                                break;
                            case 2:
                                AddRatingType();
                                break;
                            case 3:
                                AddAdvertType();
                                break;
                            case 4:
                                AddUser();
                                break;
                            case 5:
                                AddAdvert();
                                break;
                            case 6:
                                AddRating();
                                break;
                            case 7:
                                CountCity();
                                break;
                            case 8:
                                FindUsersByName();
                                break;
                            case 9:
                                FindAdvertsInCity();
                                break;
                            case 10:
                                FindAdvertsFromUser();
                                break;
                            case 11:
                                GetAverageRatingForUser();
                                break;
                            default:
                                Console.WriteLine("Указанного варианта не существует.");
                                break;
                        }
                    }
                    Console.Write("Хотите продолжить? (Y - да, N - нет): ");
                    quitAnswer = Console.ReadLine().Trim().ToUpper();
                }
                while (quitAnswer.Equals("Y"));
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                Console.ReadKey();
            }

        }

        private static void AskFirstQuestion()
        {
            Dictionary<byte, string> questions = new Dictionary<byte, string>();
            questions.Add(1, "Заполнить справочник городов");
            questions.Add(2, "Заполнить справочник типов оценок");
            questions.Add(3, "Заполнить справочник типов объявлений");
            questions.Add(4, "Зарегистрировать пользователя");
            questions.Add(5, "Создать объявление");
            questions.Add(6, "Оценить продавца");
            questions.Add(7, "Рассчитать количество городов с справочнике");
            questions.Add(8, "Найти всех пользователей с указанным именем");
            questions.Add(9, "Найти все объявления в указанном городе");
            questions.Add(10, "Найти все объявления указанного пользователя");
            questions.Add(11, "Найти сренднюю оценку для указанного продавца");

            Console.WriteLine("Что вы хотите сделать? (Укажите число)");
            foreach (var el in questions)
            {
                Console.WriteLine(el);
            }
        }

        /// <summary>
        /// Метод сохраняет данные о городе.
        /// </summary>
        private static void AddCity()
        {
            Console.Write("Введите название города: ");
            string name = Console.ReadLine().Trim();

            using (ICityRepository repository = new CityRepository())
            {
                City city = new City() { Name = name };
                repository.Create(city);
            }
            Console.WriteLine("Город сохранен.");
        }
        
        /// <summary>
        /// Метод сохраняет данные о типе оценки.
        /// </summary>
        private static void AddRatingType()
        {
            string inputString = string.Empty;
            Console.Write("Введите числовое представление типа оценки (0 - 255): ");

            inputString = Console.ReadLine().Trim();
            if (byte.TryParse(inputString, out byte mark))
            {
                Console.Write("Введите описание для типа оценки: ");
                string description = Console.ReadLine();

                using (IRatingTypeRepository ratingTypeRepository = new RatingTypeRepository())
                {
                    RatingType ratingType = new RatingType() { Mark = mark, Description = description };
                    ratingTypeRepository.Create(ratingType);
                }
                Console.WriteLine("Тип оценки сохранен.");
            }
            else
            {
                Console.WriteLine("Введено не верное значение!");
            }
        }

        /// <summary>
        /// Метод сохраняет данные о типе объявления.
        /// </summary>
        private static void AddAdvertType()
        {
            Console.Write("Введите имя типа объявления: ");
            string name = Console.ReadLine().Trim();

            Console.Write("Введите уникальный код типа объявления: ");
            string uniqueCode = Console.ReadLine().Trim();

            using (IAdvertTypeRepository advertTypeRepository = new AdvertTypeRepository())
            {
                AdvertType advertType = new AdvertType() { UniqueCode = uniqueCode, Name = name };
                advertTypeRepository.Create(advertType);
            }
            Console.WriteLine("Тип объявления сохранен.");
        }

        /// <summary>
        /// Метод сохраняет данные о пользователе.
        /// </summary>
        private static void AddUser()
        {
            Console.Write("Введите имя пользователя: ");
            string firstName = Console.ReadLine().Trim();

            Console.Write("Введите фамилию пользователя: ");
            string lastName = Console.ReadLine().Trim();

            using (IUserRepository userRepository = new UserRepository())
            {
                User user = new User() { FirstName = firstName, LastName = lastName };
                userRepository.Create(user);
            }
            Console.WriteLine("Пользователь зарегистрирован.");
        }

        /// <summary>
        /// Метод сохраняет данные об объявлении.
        /// </summary>
        private static void AddAdvert()
        {
            User seller = null;
            City city = null;
            AdvertType advertType = null;
            Advert advert = null;

            Console.Write("Введите имя зарегистрированного продавца: ");
            string firstName = Console.ReadLine().Trim();
            Console.Write("Введите фамилию зарегистрированного продавца: ");
            string lastName = Console.ReadLine().Trim();

            seller = new User() { FirstName = firstName, LastName = lastName };
            using (IUserRepository userRepository = new UserRepository())
            {
                // Ищем пользователя
                seller.Id = userRepository.GetId(seller);

                // Если не нашли пользователя, то регистрируем его
                if (seller.Id == 0)
                {
                    seller.Id = userRepository.Create(seller);
                }
            }

            Console.Write("Введите город размещения объявления: ");
            string cityName = Console.ReadLine().Trim();

            city = new City() { Name = cityName };
            using (ICityRepository cityRepository = new CityRepository())
            {
                // Ищем город
                city.Id = cityRepository.GetId(city);

                // Если не нашли город, то создаем его
                if (city.Id == 0)
                {
                    city.Id = cityRepository.Create(city);
                }
            }

            Console.Write("Введите уникальный код типа объявления: ");
            string advertTypeUniqueCode = Console.ReadLine().Trim();

            advertType = new AdvertType() {UniqueCode = advertTypeUniqueCode };
            using (IAdvertTypeRepository advertTypeRepository = new AdvertTypeRepository())
            {
                // Ищем тип объявления
                advertType.Id = advertTypeRepository.GetId(advertType);

                // Если не нашли тип объявления, то создаем его
                if (advertType.Id == 0)
                {
                    Console.Write("Введите имя типа объявления: ");
                    advertType.Name = Console.ReadLine().Trim();

                    advertType.Id = advertTypeRepository.Create(advertType);
                }
            }

            using (IAdvertRepository advertRepository = new AdvertRepository())
            {
                Console.Write("Введите описание объявления: ");
                string description = Console.ReadLine().Trim();

                advert = new Advert() { SellerId = seller.Id, CityId = city.Id, AdvertTypeId = advertType.Id, Description = description };
                advertRepository.Create(advert);
            }
            Console.WriteLine("Объявление сохранено.");    
        }

        /// <summary>
        /// Метод сохраняет данные о рейтинге продавца.
        /// </summary>
        private static void AddRating()
        {
            User userFrom = null;
            User userTo = null;
            RatingType ratingType = null;
            Rating rating = null;

            using (IUserRepository userRepository = new UserRepository())
            {
                Console.Write("Введите Ваше имя: ");
                string userFromFirstName = Console.ReadLine().Trim();
                Console.Write("Введите Вашу фамилию: ");
                string userFromLastName = Console.ReadLine().Trim();

                userFrom = new User() { FirstName = userFromFirstName, LastName = userFromLastName };

                userFrom.Id = userRepository.GetId(userFrom);
                if (userFrom.Id == 0)
                {
                    Console.WriteLine("Вы не зарегистрированы.");
                    return;
                }

                Console.Write("Введите имя оцениваемого продавца: ");
                string userToFirstName = Console.ReadLine().Trim();
                Console.Write("Введите фамилию оцениваемого продавца продавца: ");
                string userToLastName = Console.ReadLine().Trim();

                userTo = new User() { FirstName = userToFirstName, LastName = userToLastName };

                userTo.Id = userRepository.GetId(userTo);
                if (userTo.Id == 0)
                {
                    Console.WriteLine("Такой продавец не зарегистрирован.");
                    return;
                }

            }

            using (IRatingTypeRepository ratingTypeRepository = new RatingTypeRepository())
            {
                Console.Write("Введите числовое представление типа оценки (0 - 255): ");
                string inputString = Console.ReadLine().Trim();
                byte.TryParse(inputString, out byte mark);

                ratingType = new RatingType() { Mark = mark };

                ratingType.Id = (byte)ratingTypeRepository.GetId(ratingType);

                if (ratingType.Id == 0)
                {
                    Console.WriteLine("Не найден рейтинг с таким числовым представлением типа оценки.");
                    return;
                }
            }

            if (userFrom != null && userTo != null && ratingType != null)
            {
                Console.Write("Введите комментарий к рейтингу: ");
                string comment = Console.ReadLine().Trim();

                using (IRatingRepository ratingRepository = new RatingRepository())
                {
                    rating = new Rating() { UserFromId = userFrom.Id, UserToId = userTo.Id, RatingTypeId = ratingType.Id, Comment = comment };
                    ratingRepository.Create(rating);
                }
                Console.WriteLine("Оценка продавца сохранена.");
            }

        }

        /// <summary>
        /// Метод выводит в консоль информацию о количестве городов в справочнике.
        /// </summary>
        private static void CountCity()
        {
            int count = default;
            using (ICityRepository repository = new CityRepository())
            {
                count = repository.GetAll().Count;
            }
            Console.WriteLine($"Количество городов в справочнике городов равно {count}");
        }

        /// <summary>
        /// Метод ищет всех пользователей с заданным именем.
        /// </summary>
        private static void FindUsersByName()
        {
            Console.Write("Введите имя пользователя: ");
            string firstName = Console.ReadLine().Trim();

            using (IUserRepository repository = new UserRepository())
            {
                var filteredUsers = repository.FindUsersByName(firstName);

                foreach (var u in filteredUsers)
                {
                    Console.WriteLine(u as User);
                }
            }
        }

        /// <summary>
        /// Метод возвращает все объявления в указанном городе.
        /// </summary>
        private static void FindAdvertsInCity()
        {
            Console.Write("Введите название города, в котором необходимо найти объявления: ");
            string cityName = Console.ReadLine().Trim();

            using (IAdvertRepository repository = new AdvertRepository())
            {
                var filteredAdverts = repository.GetAdvertsInCity(cityName);

                foreach (var el in filteredAdverts)
                {
                    Console.WriteLine(el);
                }
            }

        }

        /// <summary>
        /// Метод возвращает все объявления, созданные конкретным пользователем.
        /// </summary>
        private static void FindAdvertsFromUser()
        {
            Console.Write("Введите имя владельца объявлений: ");
            string firstName = Console.ReadLine().Trim();
            Console.Write("Введите фамилию владельца объявлений: ");
            string lastName = Console.ReadLine().Trim();

            var user = new User(firstName, lastName);

            using (IAdvertRepository repository = new AdvertRepository())
            {
                var filteredAdverts = repository.GetAdvertsFromUser(user);

                foreach (var el in filteredAdverts)
                {
                    Console.WriteLine(el);
                }
            }
        }

        /// <summary>
        /// Метод возвращает среднюю оценку для пользователя.
        /// </summary>
        private static void GetAverageRatingForUser()
        {
            Console.Write("Введите имя владельца объявлений: ");
            string firstName = Console.ReadLine().Trim();
            Console.Write("Введите фамилию владельца объявлений: ");
            string lastName = Console.ReadLine().Trim();

            var user = new User(firstName, lastName);

            using (IRatingRepository repository = new RatingRepository())
            {
                var avgRating = repository.GetAverageRatingForUser(user);
                Console.WriteLine($"Средняя оценка пользователя равна {avgRating}");
            }
        }
    }
}
