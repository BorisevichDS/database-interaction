﻿
use [master];
go

if not exists 
(
	 select 
		  1
	 from sys.sql_logins as l 
	 where l.name like 'User01'
)
begin
	 create login User01 with password = N'Password10'
	 , default_database = master
	 , default_language = [us_english]
	 , check_expiration = off
	 , check_policy = off;
end
go 

use [$(databasename)]
go 

if not exists 
(
	 select 
		  1 
	 from sys.database_principals 
	 where type = 'S' 
	 and name like 'User01'

)
begin 
	 create user [User01] for login [User01]; 
end
go 

if not exists 
(
	 select 
		  1
	 from sys.database_role_members as m
	 join sys.database_principals as u on m.member_principal_id = u.principal_id
	 join sys.database_principals as r on m.role_principal_id = r.principal_id
	 where u.type = 'S'
	 and u.name like 'User01'
	 and r.name like 'db_datareader'
)
begin 
	 alter role db_datareader add member [User01];
end
go 

if not exists 
(
	 select 
		  1
	 from sys.database_role_members as m
	 join sys.database_principals as u on m.member_principal_id = u.principal_id
	 join sys.database_principals as r on m.role_principal_id = r.principal_id
	 where u.type = 'S'
	 and u.name like 'User01'
	 and r.name like 'db_datawriter'
)
begin 
	 alter role db_datawriter add member [User01];
end
go

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_add_user')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_add_user to [User01]
end
go 

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_update_user')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_update_user to [User01]
end
go 

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_delete_user')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_delete_user to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_add_rating_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_add_rating_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_delete_rating_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_delete_rating_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_update_rating_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_update_rating_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_add_advert_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_add_advert_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_delete_advert_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_delete_advert_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dictionary.sp_update_advert_type')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dictionary.sp_update_advert_type to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_add_advert')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_add_advert to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_update_advert')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_update_advert to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_delete_advert')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_delete_advert to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_add_rating')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_add_rating to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_update_rating')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_update_rating to [User01]
end
GO

if not exists 
(
	 select 
		  1
	 from sys.database_permissions
	 where major_id = object_id('dbo.sp_delete_rating')
	 and permission_name = 'EXECUTE'
	 and state_desc = 'GRANT'
)
begin 
	 grant execute on dbo.sp_delete_rating to [User01]
end
GO