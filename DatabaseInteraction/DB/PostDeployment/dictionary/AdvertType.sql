﻿use [$(databasename)]
go

set nocount on;

raiserror('Заполнение справочника dictionary.advert_type...', 0, 0) with nowait;

merge dictionary.[AdvertType] as target
using 
(
	 values 
		(1, 'Личные вещи')
	 , (2, 'Транспорт')
	 , (3, 'Хобби и отдых')
	 , (4, 'Для дома и дачи')
	 , (5, 'Бытовая электроника')
) as source(id, [name])
on target.Id = source.id
when matched then 
update 
set [Name] = source.[name]

when not matched by target then 
insert
(
	 Id 
  , [Name]
)
values 
(source.id, source.[name])

when not matched by source
then delete;
GO