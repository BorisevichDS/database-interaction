﻿use [$(databasename)]
go

set nocount on;

raiserror('Заполнение справочника dictionary.RatingType...', 0, 0) with nowait;

merge dictionary.[RatingType] as target
using 
(
	 values 
		(1, 1, 'Ужасно')
	 , (2, 2, 'Плохо')
	 , (3, 3, 'Средне')
	 , (4, 4, 'Нормально')
	 , (5, 5, 'Отлично')
) as source(id, mark, [description])
on target.[Id] = source.id
when matched then 
update 
set 
	 [Mark] = source.mark
	 , [Description] = source.[description]

when not matched by target then 
insert
(
	 [Id] 
	 , [Mark]
	 , [Description]
)
values 
(source.id, source.mark, source.[description])

when not matched by source
then delete;
GO