﻿use [$(databasename)]
go

set nocount on;

raiserror('Заполнение справочника dictionaty.city...', 0, 0) with nowait;

merge dictionary.[City] as target
using 
(
	 values 
		(1, 'Москва')
	 , (2, 'Санкт-Петербург')
	 , (3, 'Казань')
	 , (4, 'Мурманск')
) as source(id, [name])
on target.Id = source.id
when matched then 
update 
set [Name] = source.name

when not matched by target then 
insert
(
	 Id 
	 , [Name]
)
values 
(source.id, source.[name])

when not matched by source
then delete;
GO