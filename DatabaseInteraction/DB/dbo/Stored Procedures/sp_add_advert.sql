﻿/*
	 Процедура создает запись об объявлении.

	 Входные параметры:
	   @seller_id			int				 - Идентификатор продавца
	 , @description		nvarchar(4000)	 - Описание объявления
	 , @city_id				int				 - Идентификатор города, где размещено объявление
	 , @advert_type_id	int				 - Тип объявления

	 Выходные параметры:
	   @id int - Идентификатор созданной записи.  
*/
CREATE procedure [dbo].[sp_add_advert]
( 
  @seller_id int 
, @description nvarchar(4000)
, @city_id int
, @advert_type_id int
, @id int out
)
as 
begin
	 set nocount on;

	 insert into dbo.[Advert]
	 (
		  	 SellerId
		  , [Description]
		  , CityId
		  , AdvertTypeId
	 )
	 values 
	 (
			 @seller_id
		  , @description
		  , @city_id
		  , @advert_type_id
	 );

	 set @id = SCOPE_IDENTITY();
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура создает запись об объявлении.

Входные параметры:
@seller_id int - Идентификатор продавца
, @description	nvarchar(4000)	 - Описание объявления
, @city_id int - Идентификатор города, где размещено объявление
, @advert_type_id	int - Тип объявления

Выходные параметры:
	 @id int - Идентификатор созданной записи.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_add_advert';

