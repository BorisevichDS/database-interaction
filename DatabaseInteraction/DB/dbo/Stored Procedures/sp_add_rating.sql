﻿
/*
	 Процедура создает запись о рейтинге пользователя.

	 Входные параметры:
	    @from_user_id    int             - Оценивающий
     , @to_user_id      int             - Оцениваемый
     , @rating_type_id  int             - Тип рейтинга
     , @comment         nvarchar(4000)  - Комментарий к оценке

	 Выходные параметры:
	   @id int - Идентификатор созданной записи. 
*/
create procedure dbo.sp_add_rating
(
	@from_user_id    int
 , @to_user_id      int
 , @rating_type_id  int
 , @comment         nvarchar(4000)
 , @id              int out
)
as
begin
	 set nocount on;

	 insert into dbo.[Rating]
    (
		    [FromUserId]
	     , [ToUserId]
	     , [RatingTypeId]
	     , [Comment]
    )
	 values
    (
		    @from_user_id
	     , @to_user_id
	     , @rating_type_id
	     , @comment
    );

    set @id = scope_identity();
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура создает запись о рейтинге пользователя.

Входные параметры:
@from_user_id    int             - Оценивающий
, @to_user_id      int             - Оцениваемый
, @rating_type_id  int             - Тип рейтинга
, @comment         nvarchar(4000)  - Комментарий к оценке

Выходные параметры:
@id int - Идентификатор созданной записи.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_add_rating';

