﻿/*
	 Процедура сохраняет информацию о пользователе.

	 Входные параметры:
		@first_name nvarchar(100) - Имя пользователя
	 , @last_name nvarchar(100) - Фамилия пользователя
	 
	 Выходные параметры:
	 , @id int - Идентификатор пользователя 
*/
CREATE procedure [dbo].[sp_add_user]
(
	 @first_name nvarchar(100)
	 , @last_name nvarchar(100)
	 , @id int out
)
as
begin 
	 set nocount on;

	 -- Ищем пользователя
	 select 
		  @id = u.[Id]
	 from dbo.[User] as u
	 where u.[FirstName] = @first_name
	 and u.[LastName] = @last_name

	 if @id is null
	 begin
		  insert into dbo.[User]
		  (
			  [FirstName]
			, [LastName]
		  )
		  values
		  (
			  @first_name
			, @last_name
		  );

		  set @id = SCOPE_IDENTITY();
	 end
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура сохраняет информацию о пользователе.

Входные параметры:
@first_name nvarchar(100) - Имя пользователя
, @last_name nvarchar(100) - Фамилия пользователя
	 
Выходные параметры:
, @id int - Идентификатор пользователя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_add_user';



