﻿

/*
	 Процедура удаляет данные об объявлении.

	 Входные параметры:
		@id int - Идентификатор созданной записи. 

	 Выходные параметры:
		  нет 
*/
CREATE procedure [dbo].[sp_delete_advert]
( 
  @id int 
)
as 
begin
	 set nocount on;

	 delete from dbo.[Advert]
	 where Id = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура удаляет данные об объявлении.

Входные параметры:
@id int - Идентификатор созданной записи. 

Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_delete_advert';

