﻿
/*
	 Процедура удаляет запись о рейтинге пользователя.

	 Входные параметры:
	    @id int - Идентификатор обновляемой записи.

	 Выходные параметры:
	    нет
*/
create procedure [dbo].[sp_delete_rating]
(
   @id int
)
as
begin
	 set nocount on;

    delete r
    from dbo.[Rating] as r 
    where r.[Id] = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура удаляет запись о рейтинге пользователя.

Входные параметры:
	 @id int - Идентификатор обновляемой записи.

Выходные параметры:
	 нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_delete_rating';

