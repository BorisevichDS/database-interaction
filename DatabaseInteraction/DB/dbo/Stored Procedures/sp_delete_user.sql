﻿
/*
	 Процедура удаляет информацию о пользователе.

	 Входные параметры:
       @id int - Идентификатор пользователя
	 
	 Выходные параметры:
        нет
*/
create procedure [dbo].[sp_delete_user]
(
	 @id int             
)
as 
begin 
    set nocount on;

    delete u 
    from dbo.[User] as u 
    where u.[Id] = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура удаляет информацию о пользователе.

Входные параметры:
 @id int - Идентификатор пользователя
	 
Выходные параметры:
 нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_delete_user';

