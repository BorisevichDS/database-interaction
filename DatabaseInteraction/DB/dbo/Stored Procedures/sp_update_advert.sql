﻿
/*
	 Процедура обновляет данные об объявлении.

	 Входные параметры:
		@id					int				 - Идентификатор созданной записи. 
	 , @seller_id			int				 - Идентификатор продавца
	 , @description		nvarchar(4000)	 - Описание объявления
	 , @city_id				int				 - Идентификатор города, где размещено объявление
	 , @advert_type_id	int				 - Тип объявления

	 Выходные параметры:
		  нет 
*/
CREATE procedure [dbo].[sp_update_advert]
( 
  @id int 
, @seller_id int 
, @description nvarchar(4000)
, @city_id int
, @advert_type_id int
)
as 
begin
	 set nocount on;

	 update dbo.[Advert]
	 set 
		    SellerId = isnull(@seller_id, SellerId)
		  , CityId = isnull(@city_id, CityId)
		  , AdvertTypeId = isnull(@advert_type_id, AdvertTypeId) 
		  , [Description] = isnull(@description, [Description])
	 where Id = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура обновляет данные об объявлении.

Входные параметры:
@id int - Идентификатор созданной записи. 
, @seller_id int - Идентификатор продавца
, @description nvarchar(4000) - Описание объявления
, @city_id int - Идентификатор города, где размещено объявление
, @advert_type_id int - Тип объявления

Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_update_advert';

