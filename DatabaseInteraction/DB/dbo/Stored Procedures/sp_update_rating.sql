﻿

/*
	 Процедура обновляет запись о рейтинге пользователя.

	 Входные параметры:
	    @id              int             - Идентификатор обновляемой записи.
     , @from_user_id    int             - Оценивающий
     , @to_user_id      int             - Оцениваемый
     , @rating_type_id  int             - Тип рейтинга
     , @comment         nvarchar(4000)  - Комментарий к оценке

	 Выходные параметры:
	    нет
*/
create procedure [dbo].[sp_update_rating]
(
   @id              int
 ,	@from_user_id    int
 , @to_user_id      int
 , @rating_type_id  int
 , @comment         nvarchar(4000)
)
as
begin
	 set nocount on;

    update r 
    set 
          [FromUserId]      = isnull(@from_user_id, [FromUserId])
        , [ToUserId]        = isnull(@to_user_id, [ToUserId])
        , [RatingTypeId]    = isnull(@rating_type_id, [RatingTypeId])
        , [Comment]           = isnull(@comment, [Comment]) 
    from dbo.[Rating] as r 
    where r.[Id] = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура обновляет запись о рейтинге пользователя.

Входные параметры:
@id int - Идентификатор обновляемой записи.
, @from_user_id int - Оценивающий
, @to_user_id int - Оцениваемый
, @rating_type_id int - Тип рейтинга
, @comment nvarchar(4000) - Комментарий к оценке

Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_update_rating';

