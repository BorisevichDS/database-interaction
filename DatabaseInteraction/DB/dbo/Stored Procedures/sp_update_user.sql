﻿/*
	 Процедура обновляет информацию о пользователе.

	 Входные параметры:
       @id int - Идентификатор пользователя
	 , @first_name nvarchar(100) - Имя пользователя
	 , @last_name nvarchar(100) - Фамилия пользователя
	 
	 Выходные параметры:
        нет
*/
create procedure dbo.sp_update_user
(
      @id           int             
    , @first_name   nvarchar(100)
    , @last_name    nvarchar(100)
)
as 
begin 
    set nocount on;

    update u 
    set 
          [FirstName] = @first_name
        , [LastName] = @last_name
    from dbo.[User] as u 
    where u.[Id] = @id;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Процедура обновляет информацию о пользователе.

Входные параметры:
 @id int - Идентификатор пользователя
 , @first_name nvarchar(100) - Имя пользователя
 , @last_name nvarchar(100) - Фамилия пользователя
	 
Выходные параметры:
 нет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'sp_update_user';

