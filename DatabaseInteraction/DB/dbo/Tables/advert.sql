﻿CREATE TABLE [dbo].[Advert]
(
	  Id int not null identity(-2147483648, 1)
	, SellerId int not null 
	, [Description] NVARCHAR(4000) not null
	, CityId int not null
	, AdvertTypeId int not null
	, constraint pk_Advert primary key(Id)
	, constraint fk_Advert_User foreign key(SellerId) references dbo.[User]([Id])
	, constraint fk_Advert_City foreign key(CityId) references dictionary.[City](Id)
	, constraint fk_Advert_AdvertType foreign key(AdvertTypeId) references dictionary.[AdvertType](Id)
)
GO

create nonclustered index ndx_Advert_SellerId on dbo.[Advert](SellerId);
GO

create nonclustered index ndx_Advert_CityId on dbo.[Advert](CityId);
go 

create nonclustered index ndx_Advert_AdvertTypeId on dbo.[Advert](AdvertTypeId);
go 

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Данные об объявлениях',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Advert'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Продавец',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Advert',
    @level2type = N'COLUMN',
    @level2name = N'SellerId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Описание объявления',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Advert',
    @level2type = N'COLUMN',
    @level2name = N'Description'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Город',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Advert',
    @level2type = N'COLUMN',
    @level2name = N'CityId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Тип объявления',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Advert',
    @level2type = N'COLUMN',
    @level2name = N'AdvertTypeId'