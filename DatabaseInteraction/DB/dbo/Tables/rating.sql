﻿CREATE TABLE [dbo].[Rating] (
    [Id]             INT				IDENTITY (-2147483648, 1) NOT NULL,
    [FromUserId]   INT            NOT NULL,
    [ToUserId]     INT            NOT NULL,
    [RatingTypeId] TINYINT        NOT NULL,
    [Comment]        NVARCHAR (4000) NOT NULL,
    [CreatedOn]     DATETIME2 (7)  CONSTRAINT [df_Rating_CreatedOn] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [pk_Rating] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [fk_Rating_RatingType] FOREIGN KEY ([RatingTypeId]) REFERENCES [dictionary].[RatingType] ([Id]),
    CONSTRAINT [fk_Rating_UserFrom] FOREIGN KEY ([FromUserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [fk_Rating_UserTo] FOREIGN KEY ([ToUserId]) REFERENCES [dbo].[User] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [ndx_Rating_RatingTypeId]
    ON [dbo].[Rating]([RatingTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [ndx_Rating_FromUserId]
    ON [dbo].[Rating]([FromUserId] ASC);


GO
CREATE NONCLUSTERED INDEX [ndx_Rating_ToUserId]
    ON [dbo].[Rating]([ToUserId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оценки/рейтинги пользователя.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оценивающий', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating', @level2type = N'COLUMN', @level2name = N'FromUserId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оцениваемый', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating', @level2type = N'COLUMN', @level2name = N'ToUserId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип оценки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating', @level2type = N'COLUMN', @level2name = N'RatingTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Комментарий к оценке', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating', @level2type = N'COLUMN', @level2name = N'Comment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Дата и время создания записи', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Rating', @level2type = N'COLUMN', @level2name = N'CreatedOn';

