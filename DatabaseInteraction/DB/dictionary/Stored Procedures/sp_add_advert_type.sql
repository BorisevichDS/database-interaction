﻿
/*
	 Процедура сохраняет информацию о типе объявления.

	 Входные параметры:
	   @name nvarchar(200) - Имя типа объявления
	 , @code nvarchar(50) - Уникальный код типа объявления
	 
	 Выходные параметры:
	 , @id int - Идентификатор созданной записи 
*/
create procedure dictionary.sp_add_advert_type
(
    @name nvarchar(200) 
    , @code nvarchar(50)
    , @id int out
)
as 
begin 
    set nocount on;
   
    select 
        @id = a.Id
    from dictionary.[AdvertType] as a 
    where a.UniqueCode = @code; 

    if @id is null
    begin 
        insert into dictionary.[AdvertType] 
        (
            [Name]
            , UniqueCode
        )
        values (@name, @code);

        set @id = SCOPE_IDENTITY();
    end
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Процедура сохраняет информацию о типе объявления.

Входные параметры:
@name nvarchar(200) - Имя типа объявления
, @code nvarchar(50) - Уникальный код типа объявления
	 
Выходные параметры:
, @id int - Идентификатор созданной записи', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_add_advert_type';

