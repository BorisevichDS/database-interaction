﻿/*
	 Процедура сохраняет информацию о типе рейтинга.

	 Входные параметры:
		@mark tinyint - Числовое представление оценки
	 , @description nvarchar(100) - Описание оценки
	 
	 Выходные параметры:
	 , @id tinyint - Идентификатор созданной записи 
*/
CREATE procedure [dictionary].[sp_add_rating_type]
(
	   @mark			  tinyint
	 , @description  nvarchar(100)
	 , @id			  tinyint			out
)
as 
begin 
	 set nocount on;

	 select 
		  @id = rt.[Id]
	 from dictionary.[RatingType] as rt 
	 where rt.[Mark] = @mark;

	 if @id is null
	 begin 
		  insert into dictionary.[RatingType]
		  (
			  [Mark]
			, [Description]
		  )
		  values
		  (
			  @mark
			, @description
		  );

		  set @id = SCOPE_IDENTITY();
	 end
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура сохраняет информацию о типе рейтинга.

Входные параметры:
@mark tinyint - Числовое представление оценки
, @description nvarchar(100) - Описание оценки
	 
Выходные параметры:
, @id tinyint - Идентификатор созданной записи', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_add_rating_type';

