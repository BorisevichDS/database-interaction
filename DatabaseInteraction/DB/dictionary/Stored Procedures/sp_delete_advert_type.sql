﻿

/*
	 Процедура удаляет информацию о типе объявления.

	 Входные параметры:
	   @code nvarchar(50) - Уникальный код типа объявления
	 
	 Выходные параметры:
	    нет
*/
create procedure [dictionary].[sp_delete_advert_type]
(
    @code nvarchar(50)
)
as 
begin 
    set nocount on;
   
    delete a 
    from dictionary.[AdvertType] as a 
    where a.UniqueCode = @code;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура удаляет информацию о типе объявления.

Входные параметры:
@code nvarchar(50) - Уникальный код типа объявления
	 
Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_delete_advert_type';

