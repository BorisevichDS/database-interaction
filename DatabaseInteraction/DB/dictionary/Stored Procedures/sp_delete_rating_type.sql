﻿/*
	 Процедура удаляет информацию о типе рейтинга.

	 Входные параметры:
		@mark tinyint - Числовое представление оценки
	 
	 Выходные параметры:
        нет
*/
CREATE procedure [dictionary].[sp_delete_rating_type]
(
	   @mark tinyint
)
as 
begin 
    set nocount on;

	 delete rt
	 from dictionary.[RatingType] as rt
	 where rt.[Mark] = @mark;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура удаляет информацию о типе рейтинга.

Входные параметры:
@mark tinyint - Числовое представление оценки
	 
Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_delete_rating_type';

