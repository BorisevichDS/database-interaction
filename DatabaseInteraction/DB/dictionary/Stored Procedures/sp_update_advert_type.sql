﻿

/*
	 Процедура обновляет информацию о типе объявления.

	 Входные параметры:
	   @name nvarchar(200) - Имя типа объявления
	 , @code nvarchar(50) - Уникальный код типа объявления
	 
	 Выходные параметры:
	    нет
*/
create procedure [dictionary].[sp_update_advert_type]
(
    @name nvarchar(200) 
    , @code nvarchar(50)
)
as 
begin 
    set nocount on;
   
    update a 
    set [Name] = @name
    from dictionary.[AdvertType] as a 
    where a.UniqueCode = @code;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура обновляет информацию о типе объявления.

Входные параметры:
@name nvarchar(200) - Имя типа объявления
, @code nvarchar(50) - Уникальный код типа объявления
	 
Выходные параметры:
нет', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_update_advert_type';

