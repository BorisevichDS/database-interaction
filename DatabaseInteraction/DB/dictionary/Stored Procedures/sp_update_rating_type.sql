﻿
/*
	 Процедура обновляет информацию о типе рейтинга.

	 Входные параметры:
		@mark tinyint - Числовое представление оценки
	 , @description nvarchar(100) - Описание оценки
	 
	 Выходные параметры:
        нет
*/
create procedure [dictionary].[sp_update_rating_type]
(
	   @mark tinyint
	 , @description nvarchar(100)
)
as 
begin 
    set nocount on;

	 update rt
	 set [Description] = @description
	 from dictionary.[RatingType] as rt
	 where rt.[Mark] = @mark;
end
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Процедура обновляет информацию о типе рейтинга.

Входные параметры:
@mark tinyint - Числовое представление оценки
, @description nvarchar(100) - Описание оценки
	 
Выходные параметры:
    нет', @level0type = N'SCHEMA', @level0name = N'dictionary', @level1type = N'PROCEDURE', @level1name = N'sp_update_rating_type';

