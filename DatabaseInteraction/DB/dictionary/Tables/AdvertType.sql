﻿CREATE TABLE [dictionary].[AdvertType]
(
	Id int identity not null 
	, [Name] nvarchar(200) not null
    , UniqueCode nvarchar(50) not null
	, CreatedOn datetime2 not null constraint df_AdvertType_CreatedOn default(sysdatetime())
	, constraint pk_AdvertType primary key(Id)
)
GO

create unique index uq_AdvertType_UniqueCode on dictionary.[AdvertType](UniqueCode);

GO 
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Справочник типов объявлений',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'AdvertType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Имя типа',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'AdvertType',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Дата и время создание записи',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'AdvertType',
    @level2type = N'COLUMN',
    @level2name = N'CreatedOn'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Уникальный код',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'AdvertType',
    @level2type = N'COLUMN',
    @level2name = N'UniqueCode'