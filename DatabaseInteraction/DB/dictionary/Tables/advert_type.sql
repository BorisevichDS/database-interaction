﻿CREATE TABLE [dictionary].[advert_type]
(
	Id int identity not null 
	, [Name] nvarchar(200) not null
    , UniqueCode nvarchar(50) not null
	, CreatedOn datetime2 not null constraint df_advert_type_CreatedOn default(sysdatetime())
	, constraint pk_advert_type primary key(Id)
)
GO

create unique index uq_advert_type_UniqueCode on dictionary.[advert_type](UniqueCode);

GO 
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Справочник типов объявлений',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'advert_type'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Имя типа',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'advert_type',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Дата и время создание записи',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'advert_type',
    @level2type = N'COLUMN',
    @level2name = N'CreatedOn'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Уникальный код',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'advert_type',
    @level2type = N'COLUMN',
    @level2name = N'UniqueCode'