﻿CREATE TABLE [dictionary].[City]
(
	Id int identity not null
	, [Name] varchar(500) not null
	, CreatedOn datetime2 not null constraint df_City_CreatedOn default(sysdatetime())
	, constraint pk_City primary key(Id)
)
GO

create unique index uq_City_Name on dictionary.[City]([Name]);

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Справочник городов',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'City'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Название города',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'City',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Дата и время создания записи',
    @level0type = N'SCHEMA',
    @level0name = N'dictionary',
    @level1type = N'TABLE',
    @level1name = N'City',
    @level2type = N'COLUMN',
    @level2name = N'CreatedOn'