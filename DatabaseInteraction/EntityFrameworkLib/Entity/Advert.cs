﻿using System;
using Common;

namespace DBAccess
{
    public partial class Advert : IAdvert
    {
        /// <summary>
        /// Идентификатор продавца (владелец объявления).
        /// </summary>
        public int SellerId { get; set; }

        /// <summary>
        /// Идентификатор города, в котором размещено объявление.
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// Идентификатор типа объявления.
        /// </summary>
        public int AdvertTypeId { get; set; }

        public override string ToString()
        {
            return $"id = {Id}, description = {Description}";
        }
    }
}
