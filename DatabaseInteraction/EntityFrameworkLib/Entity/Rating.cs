﻿using System;
using Common;

namespace DBAccess
{
    public partial class Rating : IRating
    {
        /// <summary>
        /// Идентификатор пользователя, поставившего оценку.
        /// </summary>
        public int UserFromId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, которому поставили оценку.
        /// </summary>
        public int UserToId { get; set; }

        /// <summary>
        /// Идентификатор типа оценки
        /// </summary>
        public byte RatingTypeId { get; set; }
    }
}
