﻿using System;
using System.Collections.Generic;
using Common;

namespace DBAccess
{
    public partial class User : IUser
    {
        public User(string firstname, string lastname)
        {
            FirstName = firstname;
            LastName = lastname;
        }
        public User(int id, string firstname, string lastname)
        {
            Id = id;
            FirstName = firstname;
            LastName = lastname;
        }

        public override string ToString()
        {
            return $"Id = {Id}, FirstName = {FirstName}, LastName = {LastName}";
        }
    }
}
