﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertRepository : IAdvertRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(IAdvert item)
        {
            Advert advert = item as Advert;

            using (DefaultConnection dc = new DefaultConnection())
            {
                if (advert.Seller == null)
                {
                    advert.Seller = dc.Users.Where(u => u.Id == advert.SellerId).FirstOrDefault();
                }

                if (advert.City == null)
                {
                    advert.City = dc.Cities.Where(c => c.Id == advert.CityId).FirstOrDefault();
                }

                if (advert.AdvertType == null)
                {
                    advert.AdvertType = dc.AdvertTypes.Where(adt => adt.Id == advert.AdvertTypeId).FirstOrDefault();
                }

                advert = dc.Adverts.Add(advert);
                dc.SaveChanges();
            }
            return advert.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(IAdvert item)
        {
            Advert advert = item as Advert;

            using (DefaultConnection dc = new DefaultConnection())
            {
                if (advert.Seller == null)
                {
                    advert.Seller = dc.Users.Where(u => u.Id == advert.SellerId).FirstOrDefault();
                }

                if (advert.City == null)
                {
                    advert.City = dc.Cities.Where(c => c.Id == advert.CityId).FirstOrDefault();
                }

                if (advert.AdvertType == null)
                {
                    advert.AdvertType = dc.AdvertTypes.Where(adt => adt.Id == advert.AdvertTypeId).FirstOrDefault();
                }

                dc.Adverts.Add(item as Advert);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(IAdvert item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Adverts.Remove(item as Advert);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<IAdvert> GetAll()
        {
            List<IAdvert> items = new List<IAdvert>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.Adverts)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IAdvert GetById(int id)
        {
            IAdvert item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.Adverts.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает все объявления в указанном городу.
        /// </summary>
        /// <param name="cityName">Название города, в котором необходимо найти объявления.</param>
        /// <returns></returns>
        public List<IAdvert> GetAdvertsInCity(string cityName)
        {
            List<IAdvert> items = null;

            using (DefaultConnection dc = new DefaultConnection())
            {
                items = dc.Adverts.Where(a => a.City.Name == cityName).ToList<IAdvert>();
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает все объявления, созданные конкретным пользователем.
        /// </summary>
        /// <param name="user">Владелец объявлений.</param>
        /// <returns></returns>
        public List<IAdvert> GetAdvertsFromUser(IUser user)
        {
            List<IAdvert> items = null;

            using (DefaultConnection dc = new DefaultConnection())
            {
                items = dc.Adverts.Where(a => a.Seller.FirstName == user.FirstName && 
                                              a.Seller.LastName == user.LastName).ToList<IAdvert>();
            }
            return items;
        }
    }
}
