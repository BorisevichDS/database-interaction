﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertTypeRepository : IAdvertTypeRepository
    {
         /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(IAdvertType item)
        {
            IAdvertType advertType = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                advertType = dc.AdvertTypes.Add(item as AdvertType);
                dc.SaveChanges();

            }
            return advertType.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(IAdvertType item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.AdvertTypes.Add(item as AdvertType);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(IAdvertType item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.AdvertTypes.Remove(item as AdvertType);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<IAdvertType> GetAll()
        {
            List<IAdvertType> items = new List<IAdvertType>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.AdvertTypes)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IAdvertType GetById(int id)
        {
            IAdvertType item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.AdvertTypes.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        public int GetId(IAdvertType item)
        {
            int itemId = default;
            using (DefaultConnection dc = new DefaultConnection())
            {
                itemId = dc.AdvertTypes.Where(it => it.UniqueCode == item.UniqueCode).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }
    }
}
