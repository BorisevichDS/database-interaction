﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности город.
    /// </summary>
    public class CityRepository : ICityRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(ICity item)
        {
            ICity city = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                city = dc.Cities.Add(item as City);
                dc.SaveChanges();
            }
            return city.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(ICity item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Cities.Add(item as City);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(ICity item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Cities.Remove(item as City);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<ICity> GetAll()
        {
            List<ICity> items = new List<ICity>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.Cities)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ICity GetById(int id)
        {
            ICity item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.Cities.Find(keys);
            }
            return item;
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(ICity item)
        {
            int itemId = default;
            using (DefaultConnection dc = new DefaultConnection())
            {
                itemId = dc.Cities.Where(it => it.Name == item.Name).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }

        public void Dispose()
        {
            
        }
    }
}
