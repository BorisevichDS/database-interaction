﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class RatingRepository : IRatingRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(IRating item)
        {
            Rating rating = item as Rating;
            using (DefaultConnection dc = new DefaultConnection())
            {
                if (rating.UserFrom == null)
                {
                    rating.UserFrom = dc.Users.Where(u => u.Id == rating.UserFromId).FirstOrDefault();
                }

                if (rating.UserTo == null)
                {
                    rating.UserTo = dc.Users.Where(u => u.Id == rating.UserToId).FirstOrDefault();
                }

                if (rating.RatingType == null)
                {
                    rating.RatingType = dc.RatingTypes.Where(rt => rt.Id == rating.RatingTypeId).FirstOrDefault();
                }

                rating = dc.Ratings.Add(rating);
                dc.SaveChanges();
            }
            return rating.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(IRating item)
        {
            Rating rating = item as Rating;
            using (DefaultConnection dc = new DefaultConnection())
            {
                if (rating.UserFrom == null)
                {
                    rating.UserFrom = dc.Users.Where(u => u.Id == rating.UserFromId).FirstOrDefault();
                }

                if (rating.UserTo == null)
                {
                    rating.UserTo = dc.Users.Where(u => u.Id == rating.UserToId).FirstOrDefault();
                }

                if (rating.RatingType == null)
                {
                    rating.RatingType = dc.RatingTypes.Where(rt => rt.Id == rating.RatingTypeId).FirstOrDefault();
                }

                dc.Ratings.Add(rating);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(IRating item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Ratings.Remove(item as Rating);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<IRating> GetAll()
        {
            List<IRating> items = new List<IRating>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.Ratings)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IRating GetById(int id)
        {
            IRating item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.Ratings.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает среднюю оценку для пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns></returns>
        public double GetAverageRatingForUser(IUser user)
        {
            double avgRating = default;

            using (DefaultConnection dc = new DefaultConnection())
            {
                var userRatings = dc.Ratings.Where(r => r.UserTo.FirstName == user.FirstName &&
                                                        r.UserTo.LastName == user.LastName);

                if (userRatings.Count() > 0)
                {
                    avgRating = userRatings.Average(r => r.RatingType.Mark);
                }
            }
            return avgRating;
        }
    }
}
