﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип рейтинга".
    /// </summary>
    public class RatingTypeRepository : IRatingTypeRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(IRatingType item)
        {
            IRatingType ratingType = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                ratingType = dc.RatingTypes.Add(item as RatingType);
                dc.SaveChanges();
            }
            return ratingType.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(IRatingType item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.RatingTypes.Add(item as RatingType);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(IRatingType item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.RatingTypes.Remove(item as RatingType);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<IRatingType> GetAll()
        {
            List<IRatingType> items = new List<IRatingType>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.RatingTypes)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IRatingType GetById(int id)
        {
            IRatingType item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.RatingTypes.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(IRatingType item)
        {
            int itemId = default;
            using (DefaultConnection dc = new DefaultConnection())
            {
                itemId = dc.RatingTypes.Where(it => it.Mark == item.Mark).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }
    }
}
