﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Пользователь".
    /// </summary>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Метод создает запись о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные о котором необходимо сохранить.</param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(IUser item)
        {
            IUser user = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                user = dc.Users.Add(item as User);
                dc.SaveChanges();
            }
            return user.Id;
        }

        /// <summary>
        /// Метод обновляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо обновить.</param>
        public void Update(IUser item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Users.Add(item as User);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо удалить.</param>
        public void Delete(IUser item)
        {
            using (DefaultConnection dc = new DefaultConnection())
            {
                dc.Users.Remove(item as User);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех пользователей.
        /// </summary>
        /// <returns></returns>
        public List<IUser> GetAll()
        {
            List<IUser> items = new List<IUser>();

            using (DefaultConnection dc = new DefaultConnection())
            {
                foreach (var el in dc.Users)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает пользователя по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        /// <returns></returns>
        public IUser GetById(int id)
        {
            IUser item = null;
            using (DefaultConnection dc = new DefaultConnection())
            {
                object[] keys = new object[] { id };
                item = dc.Users.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Пользователь, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(IUser item)
        {
            int itemId = default;
            using (DefaultConnection dc = new DefaultConnection())
            {
                itemId = dc.Users.Where(u => u.FirstName == item.FirstName && u.LastName == item.LastName).Select(u => u.Id).FirstOrDefault();
            }
            return itemId;
        }

        /// <summary>
        /// Метод ищет всех пользователей с заданным именем.
        /// </summary>
        /// <param name="searchedName">Имя пользователя.</param>
        /// <returns>Список пользователей с соответствующим именем.</returns>
        public List<IUser> FindUsersByName(string searchedName)
        {
            List<IUser> items = null;

            using (DefaultConnection dc = new DefaultConnection())
            {
                items = dc.Users.Where(u => u.FirstName == searchedName).ToList<IUser>();
            }
            return items;
        }
    }
}
