using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Common;

namespace DBAccess
{
    public partial class Context : DbContext
    {
        public Context()
            : base("name=DefaultConnection")
        {
        }

        public virtual DbSet<Advert> Adverts { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<AdvertType> AdvertTypes { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<RatingType> RatingTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(e => e.Adverts)
                .WithRequired(e => e.Seller)
                .HasForeignKey(e => e.SellerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Ratings)
                .WithRequired(e => e.UserFrom)
                .HasForeignKey(e => e.UserFromId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Ratings1)
                .WithRequired(e => e.UserTo)
                .HasForeignKey(e => e.UserToId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdvertType>()
                .HasMany(e => e.Adverts)
                .WithRequired(e => e.AdvertType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .HasMany(e => e.Adverts)
                .WithRequired(e => e.City)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RatingType>()
                .HasMany(e => e.Ratings)
                .WithRequired(e => e.RatingType)
                .WillCascadeOnDelete(false);
        }
    }
}
