﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertRepository : IAdvertRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(Advert item)
        {
            Advert advert = item as Advert;

            using (Context c = new Context())
            {
                if (advert.Seller == null)
                {
                    advert.Seller = c.Users.Where(u => u.Id == advert.SellerId).FirstOrDefault();
                }

                if (advert.City == null)
                {
                    advert.City = c.Cities.Where(ct => ct.Id == advert.CityId).FirstOrDefault();
                }

                if (advert.AdvertType == null)
                {
                    advert.AdvertType = c.AdvertTypes.Where(adt => adt.Id == advert.AdvertTypeId).FirstOrDefault();
                }

                advert = c.Adverts.Add(advert);
                c.SaveChanges();
            }
            return advert.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(Advert item)
        {
            Advert advert = item as Advert;

            using (Context c = new Context())
            {
                if (advert.Seller == null)
                {
                    advert.Seller = c.Users.Where(u => u.Id == advert.SellerId).FirstOrDefault();
                }

                if (advert.City == null)
                {
                    advert.City = c.Cities.Where(ct => ct.Id == advert.CityId).FirstOrDefault();
                }

                if (advert.AdvertType == null)
                {
                    advert.AdvertType = c.AdvertTypes.Where(adt => adt.Id == advert.AdvertTypeId).FirstOrDefault();
                }

                c.Adverts.Add(item as Advert);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(Advert item)
        {
            using (Context c = new Context())
            {
                c.Adverts.Remove(item as Advert);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<Advert> GetAll()
        {
            List<Advert> items = new List<Advert>();

            using (Context c = new Context())
            {
                foreach (var el in c.Adverts)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Advert GetById(int id)
        {
            Advert item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.Adverts.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает все объявления в указанном городу.
        /// </summary>
        /// <param name="cityName">Название города, в котором необходимо найти объявления.</param>
        /// <returns></returns>
        public List<Advert> GetAdvertsInCity(string cityName)
        {
            List<Advert> items = null;

            using (Context c = new Context())
            {
                items = c.Adverts.Where(a => a.City.Name == cityName).ToList<Advert>();
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает все объявления, созданные конкретным пользователем.
        /// </summary>
        /// <param name="user">Владелец объявлений.</param>
        /// <returns></returns>
        public List<Advert> GetAdvertsFromUser(User user)
        {
            List<Advert> items = null;

            using (Context c = new Context())
            {
                items = c.Adverts.Where(a => a.Seller.FirstName == user.FirstName && 
                                              a.Seller.LastName == user.LastName).ToList<Advert>();
            }
            return items;
        }
    }
}
