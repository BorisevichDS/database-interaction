﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class AdvertTypeRepository : IAdvertTypeRepository
    {
         /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(AdvertType item)
        {
            AdvertType advertType = null;
            using (Context c = new Context())
            {
                advertType = c.AdvertTypes.Add(item);
                c.SaveChanges();

            }
            return advertType.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(AdvertType item)
        {
            using (Context c = new Context())
            {
                c.AdvertTypes.Add(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(AdvertType item)
        {
            using (Context c = new Context())
            {
                c.AdvertTypes.Remove(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<AdvertType> GetAll()
        {
            List<AdvertType> items = new List<AdvertType>();

            using (Context c = new Context())
            {
                foreach (var el in c.AdvertTypes)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AdvertType GetById(int id)
        {
            AdvertType item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.AdvertTypes.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        public int GetId(AdvertType item)
        {
            int itemId = default;
            using (Context c = new Context())
            {
                itemId = c.AdvertTypes.Where(it => it.UniqueCode == item.UniqueCode).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }
    }
}
