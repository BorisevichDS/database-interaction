﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности город.
    /// </summary>
    public class CityRepository : ICityRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(City item)
        {
            City city = null;
            using (Context c = new Context())
            {
                city = c.Cities.Add(item);
                c.SaveChanges();
            }
            return city.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(City item)
        {
            using (Context c = new Context())
            {
                c.Cities.Add(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(City item)
        {
            using (Context c = new Context())
            {
                c.Cities.Remove(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<City> GetAll()
        {
            List<City> items = new List<City>();

            using (Context c = new Context())
            {
                foreach (var el in c.Cities)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public City GetById(int id)
        {
            City item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.Cities.Find(keys);
            }
            return item;
        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(City item)
        {
            int itemId = default;
            using (Context c = new Context())
            {
                itemId = c.Cities.Where(it => it.Name == item.Name).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }

        public void Dispose()
        {
            
        }
    }
}
