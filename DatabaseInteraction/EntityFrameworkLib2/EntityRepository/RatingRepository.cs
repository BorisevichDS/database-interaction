﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип объявления".
    /// </summary>
    public class RatingRepository : IRatingRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(Rating item)
        {
            Rating rating = null;
            using (Context c = new Context())
            {
                if (item.UserFrom == null)
                {
                    item.UserFrom = c.Users.Where(u => u.Id == item.UserFromId).FirstOrDefault();
                }

                if (item.UserTo == null)
                {
                    item.UserTo = c.Users.Where(u => u.Id == item.UserToId).FirstOrDefault();
                }

                if (item.RatingType == null)
                {
                    item.RatingType = c.RatingTypes.Where(rt => rt.Id == item.RatingTypeId).FirstOrDefault();
                }

                rating = c.Ratings.Add(item);
                c.SaveChanges();
            }
            return rating.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(Rating item)
        {
            using (Context c = new Context())
            {
                if (item.UserFrom == null)
                {
                    item.UserFrom = c.Users.Where(u => u.Id == item.UserFromId).FirstOrDefault();
                }

                if (item.UserTo == null)
                {
                    item.UserTo = c.Users.Where(u => u.Id == item.UserToId).FirstOrDefault();
                }

                if (item.RatingType == null)
                {
                    item.RatingType = c.RatingTypes.Where(rt => rt.Id == item.RatingTypeId).FirstOrDefault();
                }

                c.Ratings.Add(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(Rating item)
        {
            using (Context c = new Context())
            {
                c.Ratings.Remove(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех объявлений.
        /// </summary>
        /// <returns></returns>
        public List<Rating> GetAll()
        {
            List<Rating> items = new List<Rating>();

            using (Context c = new Context())
            {
                foreach (var el in c.Ratings)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Rating GetById(int id)
        {
            Rating item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.Ratings.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает среднюю оценку для пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns></returns>
        public double GetAverageRatingForUser(User user)
        {
            double avgRating = default;

            using (Context c = new Context())
            {
                var userRatings = c.Ratings.Where(r => r.UserTo.FirstName == user.FirstName &&
                                                        r.UserTo.LastName == user.LastName);

                if (userRatings.Count() > 0)
                {
                    avgRating = userRatings.Average(r => r.RatingType.Mark);
                }
            }
            return avgRating;
        }
    }
}
