﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Тип рейтинга".
    /// </summary>
    public class RatingTypeRepository : IRatingTypeRepository
    {
        /// <summary>
        /// Метод создает запись в базе данных.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(RatingType item)
        {
            RatingType ratingType = null;
            using (Context c = new Context())
            {
                ratingType = c.RatingTypes.Add(item);
                c.SaveChanges();
            }
            return ratingType.Id;
        }

        /// <summary>
        /// Метод обновляет данные в базе данных.
        /// </summary>
        /// <param name="item"></param>
        public void Update(RatingType item)
        {
            using (Context c = new Context())
            {
                c.RatingTypes.Add(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные из базы данных.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(RatingType item)
        {
            using (Context c = new Context())
            {
                c.RatingTypes.Remove(item as RatingType);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех городов.
        /// </summary>
        /// <returns></returns>
        public List<RatingType> GetAll()
        {
            List<RatingType> items = new List<RatingType>();

            using (Context c = new Context())
            {
                foreach (var el in c.RatingTypes)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод ищет город по его идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RatingType GetById(int id)
        {
            RatingType item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.RatingTypes.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Город, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(RatingType item)
        {
            int itemId = default;
            using (Context c = new Context())
            {
                itemId = c.RatingTypes.Where(it => it.Mark == item.Mark).Select(it => it.Id).FirstOrDefault();
            }
            return itemId;
        }
    }
}
