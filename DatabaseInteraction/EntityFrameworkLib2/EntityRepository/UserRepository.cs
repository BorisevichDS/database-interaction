﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace DBAccess
{
    /// <summary>
    /// Класс содержит логику для работы с БД для сущности "Пользователь".
    /// </summary>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Метод создает запись о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные о котором необходимо сохранить.</param>
        /// <returns>Идентификатор созданной записи.</returns>
        public int Create(User item)
        {
            User user = null;
            using (Context c = new Context())
            {
                user = c.Users.Add(item);
                c.SaveChanges();
            }
            return user.Id;
        }

        /// <summary>
        /// Метод обновляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо обновить.</param>
        public void Update(User item)
        {
            using (Context c = new Context())
            {
                c.Users.Add(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод удаляет данные о пользователе в БД.
        /// </summary>
        /// <param name="item">Пользователь, данные которого необходимо удалить.</param>
        public void Delete(User item)
        {
            using (Context c = new Context())
            {
                c.Users.Remove(item);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Метод возвращает список всех пользователей.
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            List<User> items = new List<User>();

            using (Context c = new Context())
            {
                foreach (var el in c.Users)
                {
                    items.Add(el);
                }
            }
            return items;
        }

        /// <summary>
        /// Метод возвращает пользователя по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        /// <returns></returns>
        public User GetById(int id)
        {
            User item = null;
            using (Context c = new Context())
            {
                object[] keys = new object[] { id };
                item = c.Users.Find(keys);
            }
            return item;
        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Метод возвращает идентификатор записи в базе данных.
        /// </summary>
        /// <param name="item">Пользователь, для которого необходимо найти информацию в базе данных.</param>
        /// <returns></returns>
        public int GetId(User item)
        {
            int itemId = default;
            using (Context c = new Context())
            {
                itemId = c.Users.Where(u => u.FirstName == item.FirstName && u.LastName == item.LastName).Select(u => u.Id).FirstOrDefault();
            }
            return itemId;
        }

        /// <summary>
        /// Метод ищет всех пользователей с заданным именем.
        /// </summary>
        /// <param name="searchedName">Имя пользователя.</param>
        /// <returns>Список пользователей с соответствующим именем.</returns>
        public List<User> FindUsersByName(string searchedName)
        {
            List<User> items = null;

            using (Context c = new Context())
            {
                items = c.Users.Where(u => u.FirstName == searchedName).ToList<User>();
            }
            return items;
        }
    }
}
